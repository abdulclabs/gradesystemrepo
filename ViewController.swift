//
//  ViewController.swift
//  GradeSystem
//
//  Created by Click Labs on 1/13/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //this is View pannel which include all results...
    @IBOutlet weak var viewDisplay: UIView!
    
    // imageView field...
    @IBOutlet weak var myImageView: UIImageView!
    
    //label variable to show error if any...
    @IBOutlet weak var nilLabel: UILabel!
    
    
    //textfields...
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var ageText: UITextField!
    @IBOutlet weak var classText: UITextField!
    @IBOutlet weak var physicsText: UITextField!
    @IBOutlet weak var chemistryText: UITextField!
    @IBOutlet weak var mathText: UITextField!
    @IBOutlet weak var csText: UITextField!
    @IBOutlet weak var englishText: UITextField!
    
    //labelfields...
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    
    //subject label fields...
    @IBOutlet weak var physicsLabel: UILabel!
    @IBOutlet weak var chemistryLabel: UILabel!
    @IBOutlet weak var mathLabel: UILabel!
    @IBOutlet weak var csLabel: UILabel!
    @IBOutlet weak var englishLabel: UILabel!
    
    //marks Label fields...
    @IBOutlet weak var totalMarksLabel: UILabel!
    @IBOutlet weak var gradeLabel: UILabel!
    @IBOutlet weak var averageLabel: UILabel!
    
    //when result button pressed then...
    @IBAction func findResult(sender: AnyObject) {
       
        //hides keyboard
        //englishText.resignFirstResponder();
        
        // handling if any or more text fields are empty...
        if nameText.text == "" || classText.text == "" || ageText.text == "" || physicsText.text == "" || chemistryText.text == "" || mathText.text == "" || csText.text == "" || englishText.text == "" {
        
        
                //hidding View or Result pannel...
                viewDisplay.hidden = true
            
                //show notice to fill empty fields...
                nilLabel.text = "Please fill empty field..."
            
                //check if any field is null, if yes then controle goes to empty field to enter first...
                    if nameText.text == "" {
                
                        nameText.becomeFirstResponder()
                    }
                    if classText.text == "" {
                
                        classText.becomeFirstResponder()
                    }
                    if ageText.text == "" {
                
                        ageText.becomeFirstResponder()
                    }
                    if physicsText.text == "" {
                
                        physicsText.becomeFirstResponder()
                    }
                    if chemistryText.text == "" {
                
                        chemistryText.becomeFirstResponder()
                    }
                    if mathText.text == "" {
                
                        mathText.becomeFirstResponder()
                    }
                    if csText.text == "" {
                        
                        csText.becomeFirstResponder()
                    }
                    if englishText.text == "" {
                
                        englishText.becomeFirstResponder()
                    }

        
        }//check Marks is less than 100 or not...
        else if(physicsText.text.toInt() > 100 || chemistryText.text.toInt() > 100 || mathText.text.toInt() > 100 || csText.text.toInt() > 100 || englishText.text.toInt() > 100){
            
            
                    //show notic to enter value between 0 to 100...
                    nilLabel.text = "Enter Marks between 0 to 100..."
            
                    //hiding the view pannel...
                    viewDisplay.hidden = true
            
                    //if marks is greater than 100 then controle goes to worng value entered in text fields...
                    if physicsText.text.toInt() > 100 {
                
                        physicsText.becomeFirstResponder()
                        
                    }
                    if chemistryText.text.toInt() > 100 {
                
                        chemistryText.becomeFirstResponder()
                        
                    }
                    if mathText.text.toInt() > 100 {
                
                        mathText.becomeFirstResponder()
                       
                    }
                    if csText.text.toInt() > 100 {
                
                        csText.becomeFirstResponder()
                
                    }
                    if englishText.text.toInt() > 100 {
                
                        englishText.becomeFirstResponder()
                        
                    }
            
                        //hide keboard...
                        englishText.resignFirstResponder();
            }
            else{
        
            //hide keboard...
            englishText.resignFirstResponder();
            
            //Unhidding View or Result pannel...
            viewDisplay.hidden = false
            
            //assign persnol information...
            nameLabel.text = nameText.text
            classLabel.text = classText.text
            ageLabel.text = ageText.text
            
            //assign marks...
            physicsLabel.text = physicsText.text
            chemistryLabel.text = chemistryText.text
            mathLabel.text = mathText.text
            csLabel.text = csText.text
            englishLabel.text = englishText.text
            
            // convert textfields to integers...
            var phynum = physicsText.text.toInt()
            var chenum = chemistryText.text.toInt()
            var mathnum = mathText.text.toInt()
            var csnum = csText.text.toInt()
            var engnum = englishText.text.toInt()
            
            //find total marks...
            var totalMark = phynum! + chenum! + mathnum! + csnum! + engnum!
            
            //assign total marks...
            var total: String = String(totalMark)
            totalMarksLabel.text = total
            
            //assign average...
            var avg = totalMark / 5
            var average: String = String(avg)
            averageLabel.text = average
            
            //assign grade...
            if avg >= 75 && avg <= 100 {
                
                gradeLabel.text = "Dictinction"
                myImageView.image=UIImage(named: "img2.jpeg")
                
            }
            else if avg >= 60 {
                
                gradeLabel.text = "First Division"
                myImageView.image=UIImage(named: "img2.jpeg")
            }
            else if avg >= 45 {
                
                gradeLabel.text = "Second Division"
                myImageView.image=UIImage(named: "img2.jpeg")
            }
            else if avg >= 33 {
                
                gradeLabel.text = "Third Division"
                myImageView.image=UIImage(named: "img2.jpeg")
            }
            else {
                
                gradeLabel.text = "Failed !"
                myImageView.image=UIImage(named: "img3.jpeg")
            }

            
            
        }
        
        

    }
    
    
    //reset the value entered...
    @IBAction func resetButton(sender: AnyObject) {
        
        //reset personal information...
        nameText.text = nil
        classText.text = nil
        ageText.text = nil
        
        //reset marks value...
        physicsText.text = nil
        chemistryText.text = nil
        mathText.text = nil
        csText.text = nil
        englishText.text = nil
        
        nilLabel.text = nil
        
        // Hide view pannel...
         viewDisplay.hidden = true
        
        //hide keboard...
        englishText.resignFirstResponder();
        
    }
    
    //hide the keyboard when touches outside of the keyboard...
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

